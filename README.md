# Thermometry in Magnetic Fields

Code for calculating and controlling temperature in high magnetic fields using resistive sensors ( especially at low temperature)  as well as the calibration of these sensors in field.

For more detailed information, please see the Wiki for the project.