# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:35:27 2020

@author: jafro
"""

# import statements
import R_T_pkg
from R_T_pkg.datafit_class import Datafit
from R_T_pkg import functions as fn

import numpy as np

import h5py

# load calibration data
CT_cal_data = np.loadtxt('Smith Cernox 1010 X65735LF.txt', skiprows =10)
T_CT_cal_data, R_CT_cal_data = CT_cal_data[:,0], CT_cal_data[:,1]

# create new datafit instance with calibration data
CT_datafit = Datafit(T_CT_cal_data, R_CT_cal_data)

# specify information unique to this dataset
CT_datafit.set_dataset_info('CT Calibration', 'Temperature', 'Kelvin', 'Resistance', 'Ohms')

# plot calibration data
CT_datafit.plot_dataset()

# initial domain is set to the min and max values found in the input data
print("Initial CT domain", CT_datafit.domain, '\n')

# set a new domain so the fit is slightly extrapolated at each end
CT_datafit.set_domain(0.050, 335)
print("New CT domain", CT_datafit.domain, '\n')

# save recommended fit order and generate several potential Chebyshev fit options
CT_rec_N = CT_datafit.find_fit()
print("Recommended CT fit order is", CT_rec_N, '\n')

# (optional) store and print info for each Chebyshev fit option
print("Chebyshev fit info", '\n')
CT_datafit.get_all_fits_info()

# for each generated fit, plot the fit results
print("Plotting all fit results", '\n')
CT_datafit.plot_all_fit_results()

# for each generated fit, plot the fractional error in temperature
print("Plotting error in temperature for all potential fits", '\n')
CT_datafit.plot_all_fits_temp_error(fn.RtoT, CT_datafit.ind_data, CT_datafit.dep_data, 'Kelvin', 'True', (-10,10))

# for each generated fit, plot the fractional error in temperature over a certain subset of temperatures
print("Plotting error in temperature over a subset of temperature data for all potential fits", '\n')
CT_datafit.plot_all_fits_temp_error_subset(fn.RtoT, CT_datafit.ind_data, CT_datafit.dep_data, 'Kelvin', [0, 100], 'True', (-10,10))

# determine an optimal fit order based on temperature error
print("Recommending another fit order based on temperature error")
CT_temp_N = CT_datafit.best_fit_order_all_temp_error([-0.005, 0.005], True)

# user decides which fit to use and store
print("Fit order",CT_rec_N, "is recommended based on coefficient convergence and/or alternation.", '\n', "Fit order", CT_temp_N, "is recommended based on error in temperature", '\n')
print("Please see graphs and printed output for more information.")
n = input('What fit order would you like to use?')
CT_N = int(n)

# this assigns the recommended Chebyshev fit and its coefficients, domain, and window to class attributes
CT_datafit.cheb_fit(CT_N)  # CT_N = 8 in orignal code

calc_R = fn.TtoR(CT_datafit.ind_data, CT_datafit.c_fit)
CT_datafit.plot_fit_error(calc_R, percent=False)


saved_fit_file = 'fits' + '.hdf5'

with h5py.File(saved_fit_file, "w") as f:
    grp_ct = f.create_group("CT_fit")
    ds_ct_c = grp_ct.create_dataset("CT coefficients", data=CT_datafit.fit_coef)
    ds_ct_d = grp_ct.create_dataset("CT domain", data=CT_datafit.fit_domain)
    ds_ct_w = grp_ct.create_dataset("CT window", data=CT_datafit.fit_window)
    # could add more datasets to hold data points, calculated fit points, error, etc.


# load zero-field R,P data
P_PH, R_CT = np.loadtxt('CT_0_kG.txt', skiprows =1, delimiter=',', unpack = True)

# new Datafit instance, mostly just to use the plotting method (no cheb fitting!)
RP_no_fit = Datafit(P_PH, R_CT)
RP_no_fit.set_dataset_info('Zero-field R,P data', 'Power', 'Watts', 'Resistance', 'Ohms')
RP_no_fit.plot_dataset()


# convert resistance to temperature
T_CT = fn.RtoT(R_CT, CT_datafit.c_fit)


# new Datafit instance for T(P) fit
PH_datafit = Datafit(P_PH, T_CT)
PH_datafit.set_dataset_info('Temperature and Platform Heater Power', 'Power', 'Watts', 'Temperature', 'Kelvin')

PH_datafit.plot_dataset()

print("Initial PH domain", PH_datafit.domain, '\n')

PH_datafit.set_domain(5E-9, 1E-2)
print("New PH domain", PH_datafit.domain, '\n')

# save recommended fit order and generate several potential Chebyshev fit options
PH_rec_N = PH_datafit.find_fit()
print("Recommended PH fit order is", PH_rec_N, '\n')

# store and print info for each Chebyshev fit option
PH_datafit.get_all_fits_info()

# for each generated fit, plot the fit results
PH_datafit.plot_all_fit_results()

# for each generated fit, plot the fractional error in temperature
PH_datafit.plot_all_fits_temp_error(fn.PtoT, PH_datafit.dep_data, PH_datafit.ind_data, 'Kelvin', 'False', (-0.01, 0.01), 100)

# for each generated fit, plot the fractional error in temperature over a certain subset of temperatures
PH_datafit.plot_all_fits_temp_error_subset(fn.PtoT, PH_datafit.dep_data, PH_datafit.ind_data, 'Kelvin', [0.5,5], 'False', (-0.1, 0.1), 100)

# determine an optimal fit order based on temperature error
PH_temp_N = PH_datafit.best_fit_order_all_temp_error([-0.025, 0.025], True)

# user decides which fit to store and use
print("Fit order",PH_rec_N, "is recommended based on coefficient convergence and/or alternation.", '\n', "Fit order", PH_temp_N, "is recommended based on error in temperature", '\n')
print("Please see graphs and printed output for more information.")
n = input('What fit order would you like to use?')
PH_N = int(n)

# this assigns the recommended Chebyshev fit and its coefficients, domain, and window to class attributes
PH_datafit.cheb_fit(PH_N)  # PH_N = 7, same as original code

# this code stores both CT_fit and PH_fit in the same file
# you could also create a new hdf5 file to store each fit
with h5py.File(saved_fit_file, "r+") as f:
    grp_ph = f.create_group("PH_fit")
    ds_ph_c = grp_ph.create_dataset("PH coefficients", data=PH_datafit.fit_coef)
    ds_ph_d = grp_ph.create_dataset("PH domain", data=PH_datafit.fit_domain)
    ds_ph_w = grp_ph.create_dataset("PH window", data=PH_datafit.fit_window)
    # could add more datasets to hold data points, calculated fit points, error, etc.


