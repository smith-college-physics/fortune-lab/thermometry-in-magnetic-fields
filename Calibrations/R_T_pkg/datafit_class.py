# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:32:47 2020

@author: jafro
"""

# this is a test fit class

import numpy as np
from numpy.polynomial import Chebyshev

import matplotlib as mpl
from matplotlib import pyplot as plt

import h5py

import R_T_pkg.functions as fn

class Datafit:
    """
    Data and associated Chebyshev fit.
    
    ...
    
    Parameters
    ----------
    independent : array
        Data representing your independent variable
    dependent : array
        Data representing your dependent variable
    
    Attributes
    ----------
    ind_data : array
        Data representing the independent variable
    dep_data : array
        Data representing the dependent variable
    dep_min : float
        Smallest value present in dependent data
    dep_max : float
        Largest value present in dependent data
    range : array
        Range of dataset. Defaults to [dep_min, dep_max]
    ind_min : float
        Smallest value present in independent data
    ind_max : float
        Largest value present in independent data
    domain : array
        Domain of dataset. Defaults to [ind_min, ind_max]
    ds_title : str
        Title of the dataset
    ind_data_label : str
        Label identifying what independent data represents
    ind_data_units : str
        Unit of measurement associated with independent data
    dep_data_label : str
        Label identifying what dependent data represents
    dep_data_units : str
        Unit of measurement associated with dependent data
    c_fit : Chebyshev object
        Chebyshev fit to data
    fit_coef : array
        Coefficients of Chebyshev fit
    fit_domain : array
        Domain of Chebyshev fit
    fit_window : array
        Window of Chebyshev fit
    fit_ind_lnpoints : array
        Natural log-scaled independent data points calculated using Chebyshev fit
    fit_dep_lnpoints : array
        Natural log-scaled dependent data points calculated using Chebyshev fit
    fit_ind_points : array
        Independent data points calculated using Chebyshev fit
    fit_dep_points : array
        Dependent data points calculated using Chebyshev fit
    fractional_error : array
        Fractional error between calculated dependent data and actual dependent data
    fractional_temp_error : array
        Fractional error between fit-calculated temperature data and actual temperature data
    all_orders : array 
        Array containing all potential Chebyshev fit orders
    all_fits : dict {int : Chebyshev object,...}
        Dictionary containing all potential Chebyshev fit orders and actual fits. The key is the fit order and the value is the actual fit.
    all_fits_info : dict {int : dict {str : int, str : array, str : array, str : array, str : str}, ...}
        Nested dictionary. This structure contains a dictionary for each potential Chebyshev fit.
        The key is the fit order and the value is the dictionary containing that fit's information.
        Each fit-specfic dictionary contains the keys 'order', 'coefficients', 'domain, 'window', and 'alternates'
        For more information, see the methods 'get_fit_info()' and 'get_all_fits_info()'
    all_temp_errors : dict {int : array, ...}
        Dictionary containing fractional errors in temperature for all potential Chebyshev fit orders.
        The key is the fit order and the value is an array of fractional temperature error values
    fractional_temp_error_subset : array
        Fractional error between fit-calculated temperature data and actual temperature data over a limited subset of temperatures
    all_temp_errors_subset : dict {int : array, ...}
        Dictionary containing fractional errors in temperature over a temperature subset for all potential Chebyshev fit orders
        The key is the fit order and the value is an array of fractional temperature error values
    temp_data_subset : array
        Subset of temperature data over which fractional_temp_error_subset is calculated
    subset_domain : array
        Min and max temperature values defining temperature data subset
    all_temp_error_info : dict { int: dict {str : float, str : float, str : float},...}
        Nested dictionary. This structure contains a dictionary for each potential Chebyshev fit.
        The key is the fit order and the value is the dictionary containing information about that fit's fractional error in temperature.
        Each fit-specific dictionary contains the keys 'avg', 'min', and 'max'
        For more information, see the methods 'get_temp_error_info()' and 'get_all_temp_error_info()'
    
    """
    
    def __init__(self, independent, dependent):
        self.dep_data = dependent
        self.ind_data = independent
        
        self.dep_min = np.min(dependent)
        self.dep_max = np.max(dependent)
        self.range = [self.dep_min, self.dep_max]
        
        self.ind_min = np.min(independent)
        self.ind_max = np.max(independent)
        self.domain = [self.ind_min, self.ind_max]
        
        mpl.rc('xtick', labelsize = 18)
        mpl.rc('ytick', labelsize = 18)
        
    def set_domain(self, new_min, new_max):
        """Set the domain of the independent data
        
        Parameters
        ----------
        new_min : float
            The user-definined minimum value
        new_max : float
            The user-definined maximum value
            
        """
        
        self.domain = [new_min, new_max]
    
    def set_range(self, new_min, new_max):
        """Set the range of the dependent data
        
        Parameters
        ----------
        new_min : float
            The user-defined minimum value
        new_max : float
            The user-defined maximum value
        
        """
        
        self.range = [new_min, new_max]
    
    def set_dataset_info(self, dataset_title, ind_label, ind_units, dep_label, dep_units):
        """Set information specific to the dataset
        
        Parameters
        ----------
        dataset_title : str
            A title describing the dataset
        ind_label : str
            A label identifying what the independent data represents
        ind_units : str
            The unit of measurement associated with the independent data
        dep_label : str
            A label identifying what the dependent data represents
        dep_units : str
            The unit of measurement associated with the dependent data
        
        """
        
        self.ds_title = dataset_title
        self.ind_data_label = ind_label
        self.ind_data_units = ind_units
        self.dep_data_label = dep_label
        self.dep_data_units = dep_units
    
    # method to plot input data
    def plot_dataset(self):
        """Plot the dependent vs. independent data on a log scale"""
        
        plt.figure(figsize=(8,8))
        plt.plot(self.ind_data, self.dep_data, 'b.')
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel(self.ind_data_label +' in '+ self.ind_data_units, fontsize=18)
        plt.ylabel(self.dep_data_label +' in '+ self.dep_data_units, fontsize=18)
        plt.title(self.ds_title, fontsize=24)
        plt.grid(True)
    
    
    # method to execute chebyshev fit and return results
    def cheb_fit(self, fit_order):
        """Execute a Chebyshev fit based on the data
        
        Parameters
        ----------
        fit_order : int
            The order of the Chebyshev polynomial to fit to the data  
        
        Returns
        -------
        Chebyshev object
            The fit generated with the data
            
        """
        
        self.c_fit = Chebyshev.fit(np.log(self.ind_data),
                                   np.log(self.dep_data),
                                   fit_order,
                                   np.log(self.domain))
        
        self.fit_coef = self.c_fit.coef
        self.fit_domain = self.c_fit.domain
        self.fit_window = self.c_fit.window
        
        return self.c_fit
    
    def find_fit(self,N_initial=2, N_max=20):
        """
        Determine fit order options within a range of possibilities for eventual Chebyshev fit. 
        # TODO - add parameter and functionality for user to specify criteria like whether alternating coefficients is important
        
        Parameters
        ----------
        N_initial : int (optional)
            Minimum order, default is 2
        N_max : int (optional)
            Maximum order, default is 20
        
        Returns
        -------
        int
            The recommended order to use when generating final Chebyshev fit.
            This is the highest-order fit for which the coefficients converge AND alternate sign, 
            or the highest-order fit for which the coeffients converge if no fits are found whose coefficients alternate sign
        
        """
        
        N = N_initial
        test_fit = self.cheb_fit(N)
        lower_2_n = N_initial
        lower_1_n = N_initial
        recommended_n = N_initial
        higher_1_n = N_initial
        higher_2_n = N_initial
        last_n = N_initial
        
        if fn.series_converges_in_value(test_fit.coef) == False: 
            print("no convergence")

        else:
            while (fn.series_converges_in_value(test_fit.coef) and (N < N_max)):
                if ( (N > recommended_n) and (fn.series_alternates_sign(test_fit.coef)==True) ):
                   lower_2_n = lower_1_n
                   lower_1_n = recommended_n
                   recommended_n = N
                   #print("If statement in while loop! N =", N, '\n')   # for logical debugging
                
                elif ( (N > recommended_n) and (fn.series_alternates_sign(test_fit.coef)==False) ):
                    if (recommended_n == last_n):
                        lower_2_n = lower_1_n
                        lower_1_n = recommended_n
                        recommended_n = N
                        higher_1_n = N
                        higher_2_n = N
                        last_n = N
                        #print("If statement in elif statement in while loop! N =", N, '\n')   # for logical debugging
                    else:
                        higher_1_n = higher_2_n
                        higher_2_n = N
                        #print("Else statement in elif statement in while loop! N =", N, '\n')   # for logical debugging
                    
                N = N + 1
                test_fit = self.cheb_fit(N)
        
        # generate fit options
        lower_2_fit = self.cheb_fit(lower_2_n)
        lower_1_fit = self.cheb_fit(lower_1_n)
        recommended_fit = self.cheb_fit(recommended_n)
        higher_1_fit = self.cheb_fit(higher_1_n)
        higher_2_fit = self.cheb_fit(higher_2_n)
        
        # store fit orders and the fits themselves, avoiding storage of duplicate fits
        if ( (lower_1_n == lower_2_n) or (lower_1_n == N_initial) ):
            self.all_orders = [lower_2_n, lower_1_n, recommended_n]
            self.all_fits = {
                    lower_2_n : lower_2_fit, 
                    }
        
        elif ( (recommended_n == lower_1_n) or (recommended_n == N_initial) ):
            self.all_orders = [lower_2_n, lower_1_n, recommended_n]
            self.all_fits = {
                    lower_2_n : lower_2_fit, 
                    lower_1_n : lower_1_fit, 
                    }
        
        elif ( (higher_1_n == recommended_n) or (higher_1_n == N_initial) ):
            self.all_orders = [lower_2_n, lower_1_n, recommended_n]
            self.all_fits = {
                    lower_2_n : lower_2_fit, 
                    lower_1_n : lower_1_fit, 
                    recommended_n : recommended_fit
                    }
       
        elif ( (higher_2_n == higher_1_n) or (higher_2_n == N_initial) ):
            self.all_orders = [lower_2_n, lower_1_n, recommended_n, higher_1_n]  
            self.all_fits = {
                    lower_2_n : lower_2_fit, 
                    lower_1_n : lower_1_fit, 
                    recommended_n : recommended_fit,
                    higher_1_n : higher_1_fit
            }
        
        else:
            self.all_orders = [lower_2_n, lower_1_n, recommended_n, higher_1_n, higher_2_n]
            self.all_fits = {
                    lower_2_n : lower_2_fit,
                    lower_1_n : lower_1_fit,
                    recommended_n : recommended_fit,
                    higher_1_n : higher_1_fit,
                    higher_2_n : higher_2_fit
            }
        
        return recommended_n
    

    def get_fit_info(self, fit):
        """
        Access information about a chebyshev fit
        
        Parameters
        ----------
        fit : Chebyshev object
            The fit whose information is desired. 
        
        Returns
        -------
        dict
            Dictionary containing the fit's order, coefficients, domain, window, and whether it alternates sign
        
        """
        
        fit_info = {
                'order': np.size(fit.coef) - 1 , 
                'coefficients' : fit.coef, 
                'domain' : fit.domain,
                'window' : fit.window, 
                'alternates' : fn.series_alternates_sign(fit.coef)
        }
        
        return fit_info
    
    def get_all_fits_info(self):
        """Stores information on all saved fits"""
        
        fits_info = {}
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
            current_info = self.get_fit_info(current_fit)
        
            fits_info.update({current_order : current_info})
            print(current_order, "th order fit info", '\n', current_info, '\n')
            
        self.all_fits_info = fits_info
        
        return self.all_fits_info
        
    # method to generate points from fit
    def set_fit_points(self, fit=0):
        """Use the generated or specified Chebyshev fit to calculate regularly spaced data points."""
        
        if (fit == 0):
            fit = self.c_fit  
        
        self.fit_ind_lnpoints, self.fit_dep_lnpoints = fit.linspace(n=100)
        
        self.fit_ind_points = np.exp(self.fit_ind_lnpoints)
        self.fit_dep_points = np.exp(self.fit_dep_lnpoints)
        
    # method to plot and assess fit results
    def plot_fit_results(self, ind_pts=0, dep_pts=0, title=' Chebyshev fit'):
        """"
        Plot the data and generated Chebyshev fit on a log scale.
        
        Parameters
        ----------
        ind_pts : array (optional)
            The fit-calculated independent data to plot. 
            Default is self.fit_ind_points
        dep_pts : array (optional)
            The fit-calculated dependent data to plot.
            Default is self.fit_dep_points
        title : str (optional)
            Descriptive title for the graph
            Default is ' Chebyshev fit'
            The full graph title is the dataset title concatenated with this title
        
        """
        
        if (np.size(ind_pts) > 1):
            ind_pts = self.fit_ind_points
        if (np.size(dep_pts) > 1):
            dep_pts = self.fit_dep_points
        
        plt.figure(figsize=(8,8))
        plt.plot(self.ind_data, self.dep_data, 'b.', label='data')
        plt.plot(ind_pts, dep_pts, 'r', label='calculated from fit')
        plt.xscale('log')
        plt.yscale('log')
        plt.xlabel(self.ind_data_label +' in '+ self.ind_data_units, fontsize=18)
        plt.ylabel(self.dep_data_label +' in '+ self.dep_data_units, fontsize=18)
        plt.title(self.ds_title + title, fontsize=24)
        plt.legend(loc='best', fontsize=19)
        plt.grid(True)


    def plot_all_fit_results(self):
       """Plot the data and generated Chebyshev fit on a log scale for all potential fit order options"""
        
       for i in enumerate(self.all_orders):
           current_order = i[1]
           current_fit = self.all_fits[current_order]
           
           self.set_fit_points(current_fit)
           current_ind_points = self.fit_ind_points
           current_dep_points = self.fit_dep_points
           current_title = ' '+str(current_order)+'th order Chebyshev fit'
           
           self.plot_fit_results(current_ind_points, current_dep_points, current_title) 
    
  
    def plot_fit_error(self, calc_values, percent={False, True}):
        """
        Plot the error in the dependent data. 
        This is not the method to use for plotting delta T / T error.
        To plot delta T / T error, please see the methods 'plot_temp_error()' and 'plot_all_fits_temp_error()'
        
        Parameteters
        ------------
        calc_values : array
            Calculated dependent data values
        percent : {False, True} (optional)
            Specify whether to plot fractional error or percent error. Default is False, which plots fractional error
            
        """
       
        self.fractional_error = (calc_values - self.dep_data) / self.dep_data
        
        if (percent == False):
            error = self.fractional_error
            f_or_p = 'fractional error in ' + self.dep_data_label
        else:
            error = 100 * self.fractional_error
            f_or_p = 'percent error in ' + self.dep_data_label
       
        plt.figure(figsize = (8,8))
        plt.plot(self.ind_data, error, 'b.', label=f_or_p)
        plt.xscale('log')
        plt.xlabel(self.ind_data_label +' in '+ self.ind_data_units, fontsize = 18)
        plt.ylabel('$\Delta $'+self.dep_data_label+'$/$'+self.dep_data_label, fontsize = 18)
        plt.title(self.ds_title + '\n' + f_or_p, fontsize=24)
        plt.legend(loc = 'best',  fontsize = 18)
        plt.grid(True)
    
    
    def calc_temp_error(self, t_func, t_data, other_data, fit):
        """
        Calculate the fractional error in temperature
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
        fit : Chebyshev object
            Chebyshev fit which will be used by t_func to convert other_data into temperature
        
        Returns
        -------
        array
           Fractional error in temperature 
        
        """
        T_calc_values = t_func(other_data, fit)
        self.fractional_temp_error = (T_calc_values - t_data) / t_data
        
        return self.fractional_temp_error
    
    def calc_all_temp_error(self, t_func, t_data, other_data):
        """
        Calculate and store fractional error in temperature for all potential fit orders.
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
            
        Returns
        -------
        dict {int : array, ...}
            Dictionary containing the fractional error in temperature for each fit.
            The key is the fit order and the value is the fractional error in temperature
        
        """
        
        temp_errors = {}
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
           
            current_error = self.calc_temp_error(t_func, t_data, other_data, current_fit)
            
            temp_errors.update({current_order : current_error})
        
        self.all_temp_errors = temp_errors
        
        return self.all_temp_errors
    
    def calc_temp_error_subset(self, t_func, t_data, other_data, fit, domain):
        """
        Calculate the fractional error in temperature over a certain subset of temperatures
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
        fit : Chebyshev object
            Chebyshev fit which will be used by t_func to convert other_data into temperature
        domain : array
            An array with two temperature values. The temperature error will be calculated between those two values.
            If the exact values aren't present in the t_data array, it will be calculated between the two present values closest to the specifed values
        
        Returns
        -------
        array
           Fractional error in temperature for a certain subset of temperature values 
        
        """
        current_fit_order = np.size(fit.coef) - 1
        print(current_fit_order, "th order fit:", '\n')
        
        # make sure provided domain values are not out of range for the temperature data
        if (domain[0] < min(t_data)):
            min_temp = min(t_data)
            min_index = 0
            print("Provided minimum is lower than actual minimum. Subset minimum set to", min_temp, '\n')
            min_found = True
        elif (domain[0] > max(t_data)):
            min_temp = min(t_data)
            min_index = 0
            print("Provided minimum is higher than dataset maximum. Subset minimum set to", min_temp, '\n')
            min_found = True
        else:
            min_found = False
        
        if (domain[1] > max(t_data)):
            max_temp = max(t_data)
            max_index = (np.size(t_data) - 1)
            print("Provided maximum is higher than actual maximum. Subset maximum set to", max_temp, '\n')
            max_found = True
        elif (domain[1] < min(t_data)):
            max_temp = max(t_data)
            max_index = (np.size(t_data) - 1)
            print("Provided maximum is lower than dataset minimum. Subset maximum set to", max_temp, '\n')
            max_found = True
        else:
            max_found = False

        # iterate through t_data to find subset min/max values in temperature data
        for i in enumerate(t_data):
            current_index = i[0]
            current_temp = i[1]
            
            # check whether min and max need to be found - may have been set outside of loop
            if (min_found == True) and (max_found == True):
                break
            
            # find subset minimum
            elif (min_found == False):
                
                if (current_temp == domain[0]):
                    min_temp = current_temp
                    min_index = current_index
                    min_found = True
                    
                elif (current_temp < domain[0]):
                    lower_temp = current_temp
                    lower_index = current_index
                    lower_diff = abs(domain[0] - lower_temp)
                    
                elif (current_temp > domain[0]):
                    higher_temp = current_temp
                    higher_index = current_index
                    higher_diff = abs(domain[0] - higher_temp)
                    
                    if (higher_diff > lower_diff):
                        min_temp = lower_temp
                        min_index = lower_index
                        
                    else:
                        min_temp = higher_temp
                        min_index = higher_index
                    
                    print("Provided minimum", domain[0], "not found in dataset. Subset minimum set to", min_temp, '\n')
                    min_found = True
            
            # find subset maximum
            elif (min_found == True) and (max_found == False):
                
                if (current_temp == domain[1]):
                    max_temp = current_temp
                    max_index = current_index
                    max_found = True
                    
                elif (current_temp < domain[1]):
                    lower_temp = current_temp
                    lower_index = current_index
                    lower_diff = abs(domain[1] - lower_temp)
                    
                elif(current_temp > domain[1]):
                    higher_temp = current_temp
                    higher_index = current_index
                    higher_diff = abs(domain[1] - higher_temp)
                    
                    if (higher_diff > lower_diff):
                        max_temp = lower_temp
                        max_index = lower_index
                        
                    else:
                        max_temp = higher_temp
                        max_index = higher_index
                    
                    print("Provided maximum", domain[1], "not found in dataset. Subset maximum set to", max_temp, '\n')
                    max_found = True
            
            # if something breaks in the logic, then the subset min and max values will be set to the min and max temperatures in the dataset 
            else:
                print("lp0 on fire")   # indicates an unknown error requiring further investigation
                print("Subset minimum set to dataset minimum", min_temp, "and subset maximum set to dataset maximum", max_temp, '\n')
                min_temp = min(t_data)
                min_index = 0
                max_temp = max(t_data)
                max_index = (np.size(t_data) - 1)
                
                
        t_data_subset = t_data[min_index : max_index] 
        other_data_subset = other_data[min_index : max_index]
        
        t_calc_subset = t_func(other_data_subset, fit)
        self.fractional_temp_error_subset = (t_calc_subset - t_data_subset) / t_data_subset
        self.temp_data_subset = t_data_subset
        self.subset_domain = [min_temp, max_temp]
        
        print("Fractional error in temperature calculated between", min_temp, "and", max_temp, '\n')
        
        return self.fractional_temp_error_subset
        
    def calc_all_temp_error_subset(self, t_func, t_data, other_data, domain):
        """
        Calculate the fractional error in temperature over a certain subset of temperatures
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
        domain : array
            An array with two temperature values. The temperature error will be calculated between those two values.
            If the exact values aren't present in the t_data array, it will be calculated between the two present values closest to the specifed values
        
        """
        subset_temp_errors = {}
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
           
            current_error = self.calc_temp_error_subset(t_func, t_data, other_data, current_fit, domain)
            
            subset_temp_errors.update({current_order : current_error})
        
        self.all_temp_errors_subset = subset_temp_errors
        
        return self.all_temp_errors_subset
    
    def plot_temp_error(self, t_error, T_data_values, T_units, percent={'False', 'True'}, yrange=(None, None), mark=None, title=' Chebyshev fit'):
        """
        Plot the fractional error in temperature
       
        Parameters
        ----------
        t_error : array
            Fractional error in temperature
        T_data_values : array
            Temperature data. Recommended: self.ind_data or self.dep_data
        T_units : str
            Units of temperature associated with the data
        percent : {'False', 'True'} (optional)
            Specify whether error should be fractional or percent. Default is fractional.
        yrange : (float, float) (optional)
            Specify the limits of the graph's y-axis. Default is (None, None), which autoscales the y-axis
        mark : int (optional)
            Graphing specification. Plot every [mark]th sample. Default is None, which plots all samples
        title : str (optional)
            Descriptive title for the graph
            Default is ' Chebyshev fit'
            The full graph title is the dataset title concatenated with 'Fractional' or 'Percent','error in temperature', and this title
            
        """
        
        if (percent == 'False'):
            
            error = t_error
            
            plt.figure(figsize=(8,8))
            plt.title(self.ds_title + '\n'+' Fractional error in temperature ' + title, fontsize=20)
            plt.plot(T_data_values, error, 'b.', label='fractional error in temperature fit to data', markevery=mark)
            plt.xscale('log')
            plt.xlabel('Temperature in '+ T_units, fontsize = 18)
            plt.ylabel('$\Delta $ T / T', fontsize = 18)
            plt.ylim(yrange)
            plt.legend(loc = 'best',  fontsize = 18)
            plt.grid(True)
        
        else:
            
            error = 100 * t_error
            
            plt.figure(figsize=(8,8))
            plt.title(self.ds_title + '\n'+' Percent error in temperature ' + title, fontsize=20)
            plt.plot(T_data_values, error, 'b.', label='percent error in temperature fit to data', markevery=mark)
            plt.xscale('log')
            plt.xlabel('Temperature in '+ T_units, fontsize = 18)
            plt.ylabel('$\Delta $ T / T', fontsize = 18)
            plt.ylim(yrange)
            plt.legend(loc = 'best',  fontsize = 18)
            plt.grid(True)
            
    def plot_all_fits_temp_error(self, t_func, t_data, other_data, t_units, percent={'False','True'}, yrange=(None, None), mark=None, title=' Chebyshev fit'):
        """
        Plot the error in temperature for all potential Chebyshev fits
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
        t_units : str
            Units of temperature associated with the data
        percent : {'False', 'True'}
            Specifiy whether error should be fractional or percent. Default is fractional.
        yrange : tuple (optional)
            Specify the limits of the graph's y-axis. Default is (None, None), which autoscales the y-axis
        mark : int
            Graphing specification. Plot every [mark]th sample. Default is None, or plot all samples
        title : str (optional)
            Descriptive title for the graph
            Default is ' Chebyshev fit'
            The full graph title is the dataset title concatenated with'Fractional' or 'Percent','error in temperature', and this title
        
        """
       
        self.calc_all_temp_error(t_func, t_data, other_data)
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
            current_title = str(current_order)+'th order Chebyshev fit'
            current_temp_error = self.all_temp_errors[current_order]
            
            self.plot_temp_error(current_temp_error, t_data, t_units, percent, yrange, mark, current_title)


    def plot_all_fits_temp_error_subset(self, t_func, t_data, other_data, t_units, domain, percent={'False', 'True'}, yrange=(None, None), mark=None, title=' Chebyshev fit over subset'):
        """
        Plot the functional error in temperature over a certain temperature subset for all potential Chebyshev fits
        
        Parameters
        ----------
        t_func : function
            The function which converts non-temperature data into temperature data
            Must be of the form t_func(non-temperature data, Chebyshev fit)
        t_data : array
            The temperature data which the fit-calculated temperatures will be compared to
        other_data : array
            Non-temperature data to be converted into temperature values using the Chebyshev fits
        t_units : str
            Units of temperature associated with the data
        domain : array
            Array containing two temperature values. The fractional error in temperature will be calculated for temperatures between these two values
        percent : {'False', 'True'}
            Specifiy whether error should be fractional or percent. Default is fractional.
        yrange : tuple (optional)
            Specify the limits of the graph's y-axis. Default is (None, None), which autoscales the y-axis
        mark : int
            Graphing specification. Plot every [mark]th sample. Default is None, or plot all samples
        title : str (optional)
            Descriptive title for the graph
            Default is ' Chebyshev fit over subset'
            The full graph title is the dataset title concatenated with'Fractional' or 'Percent','error in temperature', and this title
        
        """
        self.calc_all_temp_error_subset(t_func, t_data, other_data, domain)
        temp_subset = self.temp_data_subset
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
            current_title = str(current_order)+'th order Chebyshev fit over subset'
            current_temp_error_subset = self.all_temp_errors_subset[current_order]
            
            self.plot_temp_error(current_temp_error_subset, temp_subset, t_units, percent, yrange, mark, current_title)
        

    def get_temp_error_info(self, error):
        """
        Save information about temperature error
        
        Parameters
        ----------
        error : array
            Array containing the fractional error in temperature
        
        Returns
        -------
        dict {str : float, str : float, str : float}
            Dictionary containing information on the fractional error in temperature. Keys and values are:
                'avg' : the average fractional error
                'min' : minimum error value - NOT necessarily error value closest to zero
                'max' : maximum error value - NOT necessarily error value farthest from zero
        
        """
        
        avg_t_error = np.mean(error)
        min_t_error = np.min(error)
        max_t_error = np.max(error)
        
        temp_error_info = {
                'avg' : avg_t_error,
                'min' : min_t_error,
                'max' : max_t_error
                }
        
        return temp_error_info
        

    def get_all_temp_error_info(self, subset={False, True}):
        """
        Save and print information about the fractional temperature for all potential Chebyshev fits
        
        Parameters
        ----------
        subset : {False, True} (optional)
            Parameter specifying whether to retrieve error info over all temperature data or over a subset of temperature data.
            Default value is False, which retrieves error info over all temperature data.
        
        Returns
        -------
        dict {int : dict {str : float, str : float, str : float}, ...}
            Dictionary containing information on the fractional error in temperature for each potential Chebyshev fit.
            The key is the fit order and the value is a dictionary with the information on the fractional error.
            For the dictionaries associated with each fit order, keys and values are:
                'avg' : the average fractional error
                'min' : minimum error value - NOT necessarily error value closest to zero
                'max' : maximum error value - NOT necessarily error value farthest from zero
            If parameter 'subset' is False, then this information covers fractional error over all temperature data.
            If parameter 'subset' is True, then this information only covers fractional error over a certain subset of temperature data. 
            
        """
        
        all_info = {}
        
        if (subset == False):
            
            for i in enumerate(self.all_orders):
                current_order = i[1]
                current_fit = self.all_fits[current_order]
                current_error = self.all_temp_errors[current_order]
                
                current_info = self.get_temp_error_info(current_error)
                
                all_info.update({current_order : current_info})
                
                print(current_order, "th order temp error info is", current_info, '\n')
                
            self.all_temp_error_info = all_info
            return self.all_temp_error_info
        
        else:
            for i in enumerate(self.all_orders):
                current_order = i[1]
                current_fit = self.all_fits[current_order]
                current_error = self.all_temp_errors_subset[current_order]
                
                current_info = self.get_temp_error_info(current_error)
                
                all_info.update({current_order : current_info})
                
                print(current_order, "th order temp error info is", current_info, '\n')
                
            self.all_temp_error_info_subset = all_info
            return self.all_temp_error_info_subset
    
    def best_fit_order_all_temp_error(self, band, subset={False, True}):
        """
        Compare the fractional error in temperature for all potential Chebyshev fits and determine the recommended fit based on temperature error.
        The recommended fit is the lowest-order fit whose temperature error values are within a user-defined band
        
        Parameters
        ----------
        band : array [min, max]
            Minimum and maximum acceptable values for fractional error in temperature
        subset : {False, True} (optional)
            Parameter specifying whether to assess error info over all temperature data or over a subset of temperature data.
            Default value is False, which assesses error info over all temperature data.
        
        Returns
        -------
        int
            The recommended fit order.
            This is the lowest-order fit whose temperature error values are within a user-defined band
            
        """
        
        optimal_fit_order = 0
        optimal_diff = 1.0
        
        if (subset == False):
            self.get_all_temp_error_info()
            error_info = self.all_temp_error_info
        else:
            self.get_all_temp_error_info(subset)
            error_info = self.all_temp_error_info_subset
        
        order_found = False
        
        for i in enumerate(self.all_orders):
            current_order = i[1]
            current_fit = self.all_fits[current_order]
            current_info = error_info[current_order]
            
            current_avg = current_info['avg']
            current_min = current_info['min']
            current_max = current_info['max']
            
            # check if temperature error values are within a user-defined band
            if( ((current_min > band[0]) or (current_min == band[0])) and ((current_max < band[1]) or (current_max == band[1])) ):
                # fit order is checked in increasing order, so the first fit to pass the band condition will be the lowest-order fit to do so
                if (order_found == False):
                    optimal_fit_order = current_order
                    optimal_error_info = current_info
                    optimal_avg = current_avg
                    order_found = True
            
        # check whether any fits were found to have acceptable fractional error in temperature 
        if (optimal_fit_order == 0):
            print("No potential fits meet the criteria for acceptable fractional error in temperature. Try evaluating a subset of temperature data, or widening your acceptable band.")
        else:
            print("Recommended fit order is", optimal_fit_order, '\n')
        
        return optimal_fit_order
            
# TODO - could include more methods for the derivative fit - could that be a child class? 

