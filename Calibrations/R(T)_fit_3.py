# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 08:53:17 2020

@author: jafro
"""

# IMPORT PYTHOON PACKAGES

# import custom functions
import R_T_pkg as rtpckg
from R_T_pkg import functions as fn

# import numerical Python Spackage
import numpy as np

# import Chebyshev polynomial routines
from numpy.polynomial import Chebyshev
# import numpy.polynomial.polynomial as poly

# import nonlinear curve fitting routine from SciPy package
from scipy.optimize import curve_fit

# import HDF5 file interface
import h5py

# import mpmath module for Pade approximations
import mpmath as mp

# import Matplotlib graphing package
import matplotlib as mpl
from matplotlib import pyplot as plt #this is the traditional method

mpl.rc('xtick', labelsize = 18)      #use 18 point font for numbering on x axis
mpl.rc('ytick', labelsize = 18)      #use 18 point font for numbering on y axis

# IMPORT T(P_ph) CALIBRATION DATA

# file_folder = 'calibrations/'
file_folder = ''
file_name = 'fits' 
hdf5_ph_file = file_name + '.hdf5'

with h5py.File(hdf5_ph_file, "r") as f_in:
    print(f_in.filename)
    print(f_in.visit(fn.printname), '\n')
    
    ph_coeff = f_in["PH_fit"]["PH coefficients"][:]
    ph_dom = f_in["PH_fit"]["PH domain"][:]
    ph_win = f_in["PH_fit"]["PH window"][:]
    
    PH_fit = Chebyshev(ph_coeff, ph_dom, ph_win)


print("PH_fit",PH_fit, '\n')
print(np.exp(PH_fit.domain), 'Watts')
print(np.exp(PH_fit(PH_fit.domain)), 'Kelvin', '\n')

# testing
print('100 nW corresponds to ' , np.exp(PH_fit(np.log(1E-7))), 'K')
print('  1 uW corresponds to ' , np.exp(PH_fit(np.log(1E-6))), 'K')
print(' 10 uW corresponds to ' , np.exp(PH_fit(np.log(1E-5))), 'K')    
print('100 uW corresponds to ' , np.exp(PH_fit(np.log(1E-4))), 'K')    
print('  1 mW corresponds to ' , np.exp(PH_fit(np.log(1E-3))), 'K', '\n')    


# APPLY TO FIELD DATA

# zero field test

### Let's start by building up arrays of field values and coefficients
CT_fields = np.array([])

# denote field values in kGauss (so that they will be integers)
field_values = np.array([0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 65, 70, 75, 80, 90, 100, 105, 110, 115, 120, 130, 140, 150, 160, 170, 180])

field = field_values[0]
field_string_in_kG = "%d" %field_values[0] 

input_file = 'CT_' + field_string_in_kG + '_kG.txt'
print("input file:", input_file, '\n')

#start with zero field data (collected at field of 0.034 T to correct for trapped flux)

# load file
# use unpack = True to transpose the data from x_0, y_0, x_1, y_1, ... to x_0, x_1, ... followed by y_0, y_1, ...
P_CT_0_kG, R_CT_0_kG = np.loadtxt(input_file, skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_0_kG = np.exp(PH_fit(np.log(P_CT_0_kG)))  # use new T(P_PH) calibration to find T values

T_CT_0_limits = np.array([np.min(T_CT_0_kG), np.max(T_CT_0_kG)])
print("T_CT_0_limits", T_CT_0_limits, 'Kelvin', '\n')

# choose 50 mK to 10 K for domain of fits to R(T,B) in field 
T_domain = np.array([0.050, 10.0])
CT_0_kG_fit = fn.fit_Chebyshev_to_data(T_CT_0_kG, R_CT_0_kG, 6, T_domain)
print("series converges =",fn.series_converges_in_value(CT_0_kG_fit.coef))
print("series alternates sign =",fn.series_alternates_sign(CT_0_kG_fit.coef), '\n')

print('Coefficients:', CT_0_kG_fit.coef, '\n')
print('Domain:', np.exp(CT_0_kG_fit.domain), '\n')
print('Window:', CT_0_kG_fit.window, '\n')

# plot results 
CT_0_kG_lnT, CT_0_kG_lnR = CT_0_kG_fit.linspace(n=100)
CT_T_0_kG_fit = np.exp(CT_0_kG_lnT)
CT_R_0_kG_fit = np.exp(CT_0_kG_lnR)

plt.figure(figsize = (6,6))
plt.plot(CT_T_0_kG_fit, CT_R_0_kG_fit, 'r')
plt.plot(T_CT_0_kG, R_CT_0_kG, 'b.', markevery = 250)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Temperature [K]')
plt.ylabel('Resistance [$\Omega$]')
plt.xlim(0.050, 10)
plt.ylim(100, 1E7)
plt.title('CT at 0 kG')
plt.grid(True)


# all fields

CT_fits = fn.process_power_ramp_data(field_values, PH_fit, T_domain)

# quick output checks
print("CT_fits['B_value']", CT_fits['B_value'])
print("CT_fits[0]", CT_fits[0])
print("CT_fits[1]", CT_fits[1])
print("CT_fits[-1]", CT_fits[-1], '\n')
print("CT_fits['B_fit'][0].coef[0]", CT_fits['B_fit'][0].coef[0])
print("CT_fits['B_fit'][0].coef[1]", CT_fits['B_fit'][0].coef[1])
print("CT_fits['B_fit'][0].coef[-1]", CT_fits['B_fit'][0].coef[-1])
print("CT_fits['B_fit'][-1].coef[0]", CT_fits['B_fit'][-1].coef[0], '\n')

# plot some representative traces
P_CT_10_kG, R_CT_10_kG = np.loadtxt('CT_10_kG.txt', skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_10_kG = np.exp(PH_fit(np.log(P_CT_10_kG)))  # use new T(P_PH) calibration to find T values

CT_10_kG_lnT, CT_10_kG_lnR = CT_fits['B_fit'][2].linspace(n=100)
CT_T_10_kG_fit = np.exp(CT_10_kG_lnT)
CT_R_10_kG_fit = np.exp(CT_10_kG_lnR)


P_CT_25_kG, R_CT_25_kG = np.loadtxt('CT_25_kG.txt', skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_25_kG = np.exp(PH_fit(np.log(P_CT_25_kG)))  # use new T(P_PH) calibration to find T values

CT_25_kG_lnT, CT_25_kG_lnR = CT_fits['B_fit'][5].linspace(n=100)
CT_T_25_kG_fit = np.exp(CT_25_kG_lnT)
CT_R_25_kG_fit = np.exp(CT_25_kG_lnR)


P_CT_50_kG, R_CT_50_kG = np.loadtxt('CT_50_kG.txt', skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_50_kG = np.exp(PH_fit(np.log(P_CT_50_kG)))  # use new T(P_PH) calibration to find T values

CT_50_kG_lnT, CT_50_kG_lnR = CT_fits['B_fit'][10].linspace(n=100)
CT_T_50_kG_fit = np.exp(CT_50_kG_lnT)
CT_R_50_kG_fit = np.exp(CT_50_kG_lnR)


P_CT_100_kG, R_CT_100_kG = np.loadtxt('CT_100_kG.txt', skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_100_kG = np.exp(PH_fit(np.log(P_CT_100_kG)))  # use new T(P_PH) calibration to find T values

CT_100_kG_lnT, CT_100_kG_lnR = CT_fits['B_fit'][17].linspace(n=100)
CT_T_100_kG_fit = np.exp(CT_100_kG_lnT)
CT_R_100_kG_fit = np.exp(CT_100_kG_lnR)


P_CT_150_kG, R_CT_150_kG = np.loadtxt('CT_150_kG.txt', skiprows =1, delimiter=',', unpack = True)  # load data
T_CT_150_kG = np.exp(PH_fit(np.log(P_CT_150_kG)))  # use new T(P_PH) calibration to find T values

CT_150_kG_lnT, CT_150_kG_lnR = CT_fits['B_fit'][24].linspace(n=150)
CT_T_150_kG_fit = np.exp(CT_150_kG_lnT)
CT_R_150_kG_fit = np.exp(CT_150_kG_lnR)


plt.figure(figsize = (8,8))
plt.plot(CT_T_0_kG_fit, CT_R_0_kG_fit, color = "grey", label ='0 kG')
plt.plot(T_CT_0_kG, R_CT_0_kG, 'k.', markevery = 500)
plt.plot(CT_T_10_kG_fit, CT_R_10_kG_fit, color = "red", label = '10 kG')
plt.plot(T_CT_10_kG, R_CT_10_kG, 'k.', markevery = 500)
plt.plot(CT_T_25_kG_fit, CT_R_25_kG_fit, color = "orange", label = '25 kG')
plt.plot(T_CT_25_kG, R_CT_25_kG, 'k.', markevery = 500)
plt.plot(CT_T_50_kG_fit, CT_R_50_kG_fit, color = "yellow", label = '50 kG')
plt.plot(T_CT_50_kG, R_CT_50_kG, 'k.', markevery = 500)
plt.plot(CT_T_100_kG_fit, CT_R_100_kG_fit, color = "green", label = '100 kG')
plt.plot(T_CT_100_kG, R_CT_100_kG, 'k.', markevery = 500)
plt.plot(CT_T_150_kG_fit, CT_R_150_kG_fit, color = "blue", label = '150 kG')
plt.plot(T_CT_150_kG, R_CT_150_kG, 'k.', markevery = 500)
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('Resistance [$\Omega$]', fontsize = 18)
plt.xlim(0.050, 10)
plt.ylim(100, 1E7)
plt.legend(loc = 'best', fontsize = 18)
plt.title('CX1010 X65735LF')
plt.grid(True)



# GENERATE ARRAYS OF c_i (B) FOR EACH i

B_field = CT_fits['B_value']  
print("B_field:", B_field, '\n')

B_coef_0 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_0[index] = CT_fits['B_fit'][index].coef[0]
print("B_coeff_0:",B_coef_0, '\n')

B_coef_1 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_1[index] = CT_fits['B_fit'][index].coef[1]
print("B_coeff_1",B_coef_1, '\n')

B_coef_2 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_2[index] = CT_fits['B_fit'][index].coef[2]
print("B_coeff_2",B_coef_2, '\n')

B_coef_3 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_3[index] = CT_fits['B_fit'][index].coef[3]
print("B_coeff_3",B_coef_3, '\n')

B_coef_4 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_4[index] = CT_fits['B_fit'][index].coef[4]
print("B_coeff_4",B_coef_4, '\n')

B_coef_5 = np.zeros(len(field_values))
for index, value in enumerate(CT_fits['B_value']): 
    B_coef_5[index] = CT_fits['B_fit'][index].coef[5]
print("B_coeff_5",B_coef_5, '\n')

plt.figure(figsize = (8,8))
plt.plot(CT_fits['B_value'], B_coef_0, '.', color = 'red', label = "$c_0(B)$")
plt.plot(CT_fits['B_value'], B_coef_1, '.', color = 'orange', label = "$c_1(B)$")
plt.plot(CT_fits['B_value'], B_coef_2, '.', color = 'green',  label = "$c_2(B)$")
plt.plot(CT_fits['B_value'], B_coef_3, '.', color = 'blue',  label = "$c_3(B)$")
plt.plot(CT_fits['B_value'], B_coef_4, '.', color = 'indigo',  label = "$c_4(B)$")
plt.plot(CT_fits['B_value'], B_coef_5, '.', color = 'violet',  label = "$c_5(B)$")
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('coefficient value', fontsize = 18)
plt.xlim(0, 300)
plt.legend(loc = 'best' , fontsize = 18)
plt.grid(True)

# scaled coefficients
scaled_coef_0 = (B_coef_0 - B_coef_0[0]) / B_coef_0[0]
scaled_coef_1 = (B_coef_1 - B_coef_1[0]) / B_coef_1[0]
scaled_coef_2 = (B_coef_2 - B_coef_2[0]) / B_coef_2[0]
scaled_coef_3 = (B_coef_3 - B_coef_3[0]) / B_coef_3[0]
scaled_coef_4 = (B_coef_4 - B_coef_4[0]) / B_coef_4[0]
scaled_coef_5 = (B_coef_5 - B_coef_5[0]) / B_coef_5[0]

plt.figure(figsize = (8,6))
plt.plot(CT_fits['B_value'], scaled_coef_0,  '.', color = "red", label = '$ \Delta c_0 /  c_0$')
plt.plot(CT_fits['B_value'], scaled_coef_1,  '.', color = "orange", label = '$ \Delta c_1 /  c_1$')
plt.plot(CT_fits['B_value'], scaled_coef_2,  '.', color = 'green',label = '$ \Delta c_2 / c_2$')
plt.plot(CT_fits['B_value'], scaled_coef_3,  '.', color = "blue",label = '$ \Delta c_3 / c_3$')
plt.plot(CT_fits['B_value'], scaled_coef_4,  '.', color = "indigo",label = '$  \Delta c_4 / c_4$')
plt.plot(CT_fits['B_value'], scaled_coef_5,   '.', color = "violet",label = '$  \Delta c_5 / c_5$')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$ [c_n(B) - c_n(0)]  / c_n(0)$', fontsize = 18)
plt.xlim(0, 300)
plt.ylim(-1, 0)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)
plt.show()



# PADE FIT TO FIELD DEPENDENCE OF c_n(B)

# y_n(B) functions resemble a_n(e^(-B/B_o) - 1)
def g(x):
    return mp.exp(-x) - 1

g_Taylor = mp.taylor(g, 0, 6)
mp.nprint(g_Taylor)
1/g_Taylor[-1]

p_series_3, q_series_3 = mp.pade(g_Taylor, 3, 3)
mp.nprint(p_series_3)
mp.nprint(q_series_3)

# fit to c_0(B)

# There is an approximately linear drift in c_n(B) at high field for some values of n. To account for this, we try dropping the highest order term in the denominator

# modified exponential decay term  
def scaled_Pade_dependence_32(field, kappa1, kappa3, gamma1, gamma2):
    numerator = ( kappa1*(field) + kappa3*(field)**3)
    denominator = (1 + gamma1 * (field) + gamma2 * (field)**2)
    return numerator/denominator

# We will start with this an approximation to the fractional change in Chebyshev coefficient in field, using the coefficients suggested in the Padé expansion for initial values

# test of equation scaled_Pade_dependence_32
    
a_0 = 0.1 
initial_guess =  np.array([-1 * a_0, -1/60.0 * a_0, 1/2, 1/10]) 
boundary_values = ([ -np.inf, -np.inf,0, 0], [ 0, 0, np.inf, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_0, ScaledPadeCovariance_0  = curve_fit(
    scaled_Pade_dependence_32, scaled_field, scaled_coef_0, 
    p0 = initial_guess, bounds = boundary_values)

# generate a series of field values
B_test = np.linspace(0, 360, 2000) / B_scale  

# calculate y(B) using the function above (scaled_Pade_dependence_32)
scaled_Pade_0 = scaled_Pade_dependence_32(B_test, ScaledPadeFit_0[0],ScaledPadeFit_0[1] , 
                                         ScaledPadeFit_0[2], ScaledPadeFit_0[3])

# plot the results
plt.figure(figsize = (8,8))
#plt.plot(B_test, c_test, 'k')
plt.plot(B_test * B_scale , scaled_Pade_0, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_0, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_0(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)

# evaluate the coefficients for significance
ScaledPadeError_0 = np.sqrt(np.diag(ScaledPadeCovariance_0))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_0[0]), '±', '{:.2g}'.format(ScaledPadeError_0[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_0[1]), '±', '{:.2g}'.format(ScaledPadeError_0[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_0[2]), '±', '{:.2g}'.format(ScaledPadeError_0[2]))
print('gamma_2 =' , '{:.3g}'.format(ScaledPadeFit_0[3]), '±', '{:.2g}'.format(ScaledPadeError_0[3]))
print('\n')

# the B^2 term in the denominator doesn't seem to be significant, so let's drop it and rerun the fit with a modified Pade dependence

def scaled_Pade_dependence_31(field, kappa1, kappa3, gamma1):
    numerator = ( kappa1*(field) + kappa3*(field)**3)
    denominator = (1 + gamma1 * (field))
    return numerator/denominator

initial_guess = 0.1 * np.array([-1, -1/60.0, 1/2])
boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

# execute the fit

ScaledPadeFit_0, ScaledPadeCovariance_0  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_0, 
                                                     p0 = initial_guess, bounds = boundary_values)

# assess the result
ScaledPadeError_0 = np.sqrt(np.diag(ScaledPadeCovariance_0))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_0[0]), '±', '{:.2g}'.format(ScaledPadeError_0[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_0[1]), '±', '{:.2g}'.format(ScaledPadeError_0[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_0[2]), '±', '{:.2g}'.format(ScaledPadeError_0[2]))
print('\n')

# all the terms are significant now 

# use the modified curve fit to generate values
B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_0 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_0[0],ScaledPadeFit_0[1] , 
                                         ScaledPadeFit_0[2])

# plot the results
plt.figure()
plt.plot(B_test * B_scale , scaled_Pade_0, 'r')
plt.plot(B_field, scaled_coef_0, 'b.')
plt.xlabel('Field [kG]')
plt.ylabel('coefficient value')
plt.grid(True)


# fit to c_1(B), use fit coefficients for previous fit as initial guess

initial_guess = ScaledPadeFit_0

boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_1, ScaledPadeCovariance_1  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_1, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_1 = np.sqrt(np.diag(ScaledPadeCovariance_1))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_1[0]), '±', '{:.2g}'.format(ScaledPadeError_1[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_1[1]), '±', '{:.2g}'.format(ScaledPadeError_1[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_1[2]), '±', '{:.2g}'.format(ScaledPadeError_1[2]))
print('\n')

B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_1 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_1[0],ScaledPadeFit_1[1] , 
                                         ScaledPadeFit_1[2])

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_1, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_1, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_1(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)


# fit to c_2(B), use fit coefficients for previous fit as initial guess

initial_guess = ScaledPadeFit_1

boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_2, ScaledPadeCovariance_2  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_2, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_2 = np.sqrt(np.diag(ScaledPadeCovariance_2))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_2[0]), '±', '{:.2g}'.format(ScaledPadeError_2[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_2[1]), '±', '{:.2g}'.format(ScaledPadeError_2[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_2[2]), '±', '{:.2g}'.format(ScaledPadeError_2[2]))
print('\n')

B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_2 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_2[0],ScaledPadeFit_2[1] , 
                                         ScaledPadeFit_2[2])

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_2, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_2, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_2(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)


# fit to c_3(B), use fit coefficients for previous fit as initial guess

initial_guess = ScaledPadeFit_2

boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_3, ScaledPadeCovariance_3  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_3, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_3 = np.sqrt(np.diag(ScaledPadeCovariance_3))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_3[0]), '±', '{:.2g}'.format(ScaledPadeError_3[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_3[1]), '±', '{:.2g}'.format(ScaledPadeError_3[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_3[2]), '±', '{:.2g}'.format(ScaledPadeError_3[2]))
print('\n')

B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_3 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_3[0],ScaledPadeFit_3[1] , 
                                         ScaledPadeFit_3[2])

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_3, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_3, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_3(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)


# fit to c_4(B), use fit coefficients for previous fit as initial guess

initial_guess = ScaledPadeFit_3

boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_4, ScaledPadeCovariance_4  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_4, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_4 = np.sqrt(np.diag(ScaledPadeCovariance_4))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_4[0]), '±', '{:.2g}'.format(ScaledPadeError_4[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_4[1]), '±', '{:.2g}'.format(ScaledPadeError_4[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_4[2]), '±', '{:.2g}'.format(ScaledPadeError_4[2]))
print('\n')

B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_4 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_4[0],ScaledPadeFit_4[1] , 
                                         ScaledPadeFit_4[2])

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_4, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_4, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_4(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)


# fit to c_5(B), use fit coefficients for previous fit as initial guess

initial_guess = ScaledPadeFit_4

boundary_values = ([ -np.inf, -np.inf,0], [ 0, 0, np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_5, ScaledPadeCovariance_5  = curve_fit(scaled_Pade_dependence_31, scaled_field, scaled_coef_5, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_5 = np.sqrt(np.diag(ScaledPadeCovariance_5))
print('kappa_1 =' , '{:.3g}'.format(ScaledPadeFit_5[0]), '±', '{:.2g}'.format(ScaledPadeError_5[0]))
print('kappa_3 =' , '{:.3g}'.format(ScaledPadeFit_5[1]), '±', '{:.2g}'.format(ScaledPadeError_5[1]))
print('gamma_1 =' , '{:.3g}'.format(ScaledPadeFit_5[2]), '±', '{:.2g}'.format(ScaledPadeError_5[2]))
print('\n')

B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_5 = scaled_Pade_dependence_31(B_test, ScaledPadeFit_5[0],ScaledPadeFit_5[1] , 
                                         ScaledPadeFit_5[2])

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_5, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_5, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_5(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)

# this result implies that the kappa_3 term is not siginificant for c_5(B). we test that by defining a new function without that term

def scaled_Pade_dependence_11(field, kappa1, gamma1):
    numerator = ( kappa1*(field))
    denominator = (1 + gamma1 * (field))
    return numerator/denominator

initial_guess = np.array([-4.7e-2,  6.7e-2])

boundary_values = ([ -np.inf, 0], [ 0,  np.inf ])

B_scale = 1 # kG
scaled_field = B_field / B_scale

ScaledPadeFit_5_new, ScaledPadeCovariance_5_new  = curve_fit(scaled_Pade_dependence_11, scaled_field, scaled_coef_5, 
                                                     p0 = initial_guess, bounds = boundary_values)
ScaledPadeError_5_new = np.sqrt(np.diag(ScaledPadeCovariance_5_new))
print("ScaledPadeFit_5_new:",ScaledPadeFit_5_new, '\n')
print("ScaledPadeError_5_new:",ScaledPadeError_5_new, '\n')


B_test = np.linspace(0, 360, 2000) / B_scale

scaled_Pade_5_new = scaled_Pade_dependence_11(B_test, ScaledPadeFit_5_new[0], ScaledPadeFit_5_new[1] )

plt.figure(figsize = (8,8))
plt.plot(B_test * B_scale , scaled_Pade_5_new, 'r', label = 'Padé fit')
plt.plot(B_field, scaled_coef_5, 'b.', label = 'data')
plt.xlabel('Field [kG]', fontsize = 18)
plt.ylabel('$y_5(B)$', fontsize = 18)
plt.xlim(-20, 300)
plt.legend(loc = 'best', fontsize = 18)
plt.grid(True)

# All terms are now significant and the fit appears unchanged. Higher field data would be needed to better constrain the curve. Since this is the smallest of the coefficients in the orthogonal Chebyshev expansion, the deviation of the fit from the data above approximately 12.5 tesla will be relatively unimportant in the calculation of R(T, B)



# CALCULATE c_n(B)

# define coefficient arrays and matrices for calculation of c_n(B)
c_n_zero_coefficients = np.array([B_coef_0[0] ,B_coef_1[0] , B_coef_2[0], B_coef_3[0], B_coef_4[0], B_coef_5[0]])
print("c_n_zero_coefficients:",c_n_zero_coefficients, '\n')

print("ScaledPadeFit_0", ScaledPadeFit_0, '\n')
print("ScaledPadeFit_1", ScaledPadeFit_1, '\n')
print("ScaledPadeFit_2", ScaledPadeFit_2, '\n')
print("ScaledPadeFit_3", ScaledPadeFit_3, '\n')
print("ScaledPadeFit_4", ScaledPadeFit_4, '\n')
print("ScaledPadeFit_5", ScaledPadeFit_5, '\n')

# set terms not significantly different from zero to zero
ScaledPadeFit_5[1] = 0.0
print("ScaledPadeFit_5", ScaledPadeFit_5, '\n')

c_zero_array = c_n_zero_coefficients

kappa_shape = (6,4)
kappa_matrix = np.zeros(kappa_shape)

y_index = 0 
kappa_matrix[:,1] = np.array([ScaledPadeFit_0[y_index],ScaledPadeFit_1[y_index], ScaledPadeFit_2[y_index], 
                          ScaledPadeFit_3[y_index], ScaledPadeFit_4[y_index] , ScaledPadeFit_5[y_index]])
y_index = 1 
kappa_matrix[:,3] = np.array([ScaledPadeFit_0[y_index],ScaledPadeFit_1[y_index], ScaledPadeFit_2[y_index], 
                          ScaledPadeFit_3[y_index], ScaledPadeFit_4[y_index] , ScaledPadeFit_5[y_index]])
print("kappa matrix",kappa_matrix, '\n')

gamma_shape = (6,2)
gamma_matrix = np.ones(gamma_shape)

y_index = 2
gamma_matrix[:,1] = np.array([ScaledPadeFit_0[y_index],ScaledPadeFit_1[y_index], ScaledPadeFit_2[y_index], 
                          ScaledPadeFit_3[y_index], ScaledPadeFit_4[y_index] , ScaledPadeFit_5[y_index]])
print("gamma matrix", gamma_matrix, '\n')

print("c_zero_array", c_zero_array, '\n')

# test results!

# test function for B = 0 
fn.field_coefficients(0, c_zero_array, kappa_matrix, gamma_matrix)

# calculate coefficient values for 1 kG (0.1 T) 
fn.field_coefficients(1, c_zero_array, kappa_matrix, gamma_matrix)

# verify that c_0 values are unchanged
print("c_zero_array",c_zero_array, '\n')



# CALCULATE R(T,B) and test as desired

# accounting for field dependence requires modifying the coefficients in the fit. make a copy of the fit so the original is unchanged
print("CT_fits[0][1]", CT_fits[0][1], '\n')
CT_zero_fit = CT_fits[0][1].copy()
CT_B_fit = CT_zero_fit.copy()


# zero field

# use c_n(B) to generate chebyshev fit for zero field
CT_B_fit.coef = fn.field_coefficients(0, c_zero_array, kappa_matrix, gamma_matrix)

# test if it worked (were coefficients set?)
print("CT_B_fit.coeff", CT_B_fit.coef, '\n')
print("c_zero_array", c_zero_array, '\n')

# do some test calculations
fn.TtoR(1.0, CT_zero_fit)
fn.TtoR(1.0, CT_B_fit)


# 10 Tesla field

CT_B_fit.coef = fn.field_coefficients(100, c_zero_array, kappa_matrix, gamma_matrix)
print("CT_B_fit",CT_B_fit, '\n')

# try test T --> R calculation and its inverse R --> T
fn.TtoR(1.0, CT_B_fit)
print(np.asscalar(fn.RtoT(695.3095593701594, CT_B_fit)), '\n')



# TEST GENERAL FUNCTIONS FOR CALCULATION OF R(T, B) AND T(R, B)


# test at zero field
print(fn.TandBtoR(1.0, CT_zero_fit, kappa_matrix, gamma_matrix, 0), '\n')
print(fn.TandBtoR(1.0, CT_zero_fit, kappa_matrix, gamma_matrix), '\n')

# test at 100 kG
print(fn.TandBtoR(1.0 , CT_zero_fit, kappa_matrix, gamma_matrix, 100), '\n')

print(fn.TandBtoR(np.array([0.1, 0.2, 0.5]) , CT_zero_fit, kappa_matrix, gamma_matrix, 100), '\n')



#test at zero field
print(fn.RandBtoT(806.20334, CT_zero_fit, kappa_matrix, gamma_matrix, 0), '\n')
print(fn.RandBtoT(806.20334, CT_zero_fit, kappa_matrix, gamma_matrix),'\n')

# compare to results if mistakenly use zero field fit
#test at 100 kG
print(fn.RandBtoT(695.3095593701594, CT_zero_fit, kappa_matrix, gamma_matrix, 100), '\n')
print(fn.RandBtoT(806.2033400287328, CT_zero_fit, kappa_matrix, gamma_matrix, 100), '\n')

print(fn.RandBtoT(np.array([600, 800, 1000]), CT_zero_fit, kappa_matrix, gamma_matrix, 100), '\n')

# test results
test_R_values = np.array([650.0 , 700.0, 750.0, 800.0]) # ohms
test_B_values = np.array([40, 45.0, 50.0, 55.0 ]) # kiloGauss 
test_T_values = np.zeros(test_R_values.size)
print("test_T_values",test_T_values, '\n')

calculated_T_values = fn.calculate_T_from_data(test_R_values, test_B_values, CT_zero_fit, kappa_matrix, gamma_matrix)
print("calculated_T_values", calculated_T_values, '\n')



# SAVE FIELD-DEPENDENT FIT TO FILE

CT_kappa_matrix = kappa_matrix
CT_gamma_matrix = gamma_matrix

CT_field_calibration = (CT_zero_fit, CT_kappa_matrix, CT_gamma_matrix )

CT_field_calibration[0]
print("CT_field_calibration[0]",CT_field_calibration[0], '\n')
print("CT_field_calibration[1]",CT_field_calibration[1], '\n')
print("CT_field_calibration[2]",CT_field_calibration[2], '\n')

# file_folder = 'calibrations/'
file_folder = ''
file_name = 'CX1010_SN_X65735LF_RTB_calibration' 

hdf5_calibration_output = file_name + '.hdf5'

with h5py.File(hdf5_calibration_output, "w") as f_out:
    grp1 = f_out.create_group("RTB calibration")
    dset1a = grp1.create_dataset("RTB coefficients", data=CT_field_calibration[0].coef)
    dset1b = grp1.create_dataset("RTB domain", data=CT_field_calibration[0].domain)
    dset1c = grp1.create_dataset("RTB window", data=CT_field_calibration[0].window)
    dset1d = grp1.create_dataset("RTB kappa", data=CT_field_calibration[1])
    dset1e = grp1.create_dataset("RTB gamma", data=CT_field_calibration[2])
    
# check that everything loaded properly
with h5py.File(hdf5_calibration_output, "r") as f_out:
    print(f_out.filename)
    print(f_out.visit(fn.printname), '\n')
    
    loaded_coef = f_out["RTB calibration"]["RTB coefficients"][:]
    loaded_domain = f_out["RTB calibration"]["RTB domain"][:]
    loaded_window = f_out["RTB calibration"]["RTB window"][:]
    
    loaded_kappa = f_out["RTB calibration"]["RTB kappa"][:]
    loaded_gamma = f_out["RTB calibration"]["RTB gamma"][:]
    
    loaded_fit = Chebyshev(loaded_coef, loaded_domain, loaded_window)

print("loaded fit", '\n', loaded_fit, '\n')
print("loaded_kappa", '\n', loaded_kappa, '\n')
print("loaded_gamma", '\n', loaded_gamma, '\n')

new_T_values = fn.calculate_T_from_data(test_R_values, test_B_values, loaded_fit, loaded_kappa, loaded_gamma)
print("new_T_values",new_T_values)