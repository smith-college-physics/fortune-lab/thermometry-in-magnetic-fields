# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 15:59:36 2020
Code walkthrough with August 2013 Ba3CoSb2O9 data
@author: jafro
"""

# import statements
import R_T_pkg
from R_T_pkg.datafit_class import Datafit
from R_T_pkg import functions as fn

import numpy as np

import h5py

# load calibration data
CT_cal_data = np.loadtxt('Smith_CT_Aug2013.cal.txt', delimiter=',',skiprows =5)
T_CT_cal_data, R_CT_cal_data = CT_cal_data[:,0], CT_cal_data[:,1]

# create new datafit instance with calibration data
CT_datafit = Datafit(T_CT_cal_data, R_CT_cal_data)

# specify information unique to this dataset
CT_datafit.set_dataset_info('CT Calibration', 'Temperature', 'Kelvin', 'Resistance', 'Ohms')

# plot calibration data
CT_datafit.plot_dataset()

# initial domain is set to the min and max values found in the input data
print("Initial CT domain", CT_datafit.domain, '\n')

# set a new domain so the fit is slightly extrapolated at each end
CT_datafit.set_domain(0.040, 15)
print("New CT domain", CT_datafit.domain, '\n')

# save recommended fit order and generate several potential Chebyshev fit options
CT_rec_N = CT_datafit.find_fit(3, 8)
print("Recommended CT fit order is", CT_rec_N, '\n')

print("Chebyshev fit info", '\n')
CT_datafit.get_all_fits_info()

# for each generated fit, plot the fit results
print("Plotting all fit results", '\n')
CT_datafit.plot_all_fit_results()

# for each generated fit, plot the fractional error in temperature
#print("Plotting error in temperature for all potential fits", '\n')
#CT_datafit.plot_all_fits_temp_error(fn.RtoT, CT_datafit.ind_data, #CT_datafit.dep_data, 'Kelvin', 'False')

# for each generated fit, plot the fractional error in temperature over a certain subset of temperatures
CT_datafit.plot_all_fits_temp_error_subset(fn.RtoT, CT_datafit.ind_data, CT_datafit.dep_data, 'Kelvin', [0, 8], 'False')

# determine an optimal fit order based on temperature error
print("Recommending another fit order based on temperature error", '\n')
CT_temp_N = CT_datafit.best_fit_order_all_temp_error([-0.015, 0.015], True)

# user decides which fit to use and store
print("Fit order",CT_rec_N, "is recommended based on coefficient convergence and/or alternation.", '\n', "Fit order", CT_temp_N, "is recommended based on error in temperature", '\n')
print("Please see graphs and printed output for more information.")
n = input('What fit order would you like to use?')
CT_N = int(n)


# this assigns the recommended Chebyshev fit and its coefficients, domain, and window to class attributes
CT_datafit.cheb_fit(CT_N)

saved_fit_file = 'fits' + '.hdf5'

with h5py.File(saved_fit_file, "w") as f:
    grp_ct = f.create_group("CT_fit")
    ds_ct_c = grp_ct.create_dataset("CT coefficients", data=CT_datafit.fit_coef)
    ds_ct_d = grp_ct.create_dataset("CT domain", data=CT_datafit.fit_domain)
    ds_ct_w = grp_ct.create_dataset("CT window", data=CT_datafit.fit_window)
    # could add more datasets to hold data points, calculated fit points, error, etc.


