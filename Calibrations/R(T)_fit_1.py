# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 08:22:30 2020
From Professor Fortune's file R(T)_fit_part_1.ipynb
@author: jafro
"""
# IMPORT NEEDED PYTHON PACKAGES

# import custom functions
import R_T_pkg as rtpckg
from R_T_pkg import functions as fn

# import numerical Python Spackage
import numpy as np

# import HDF5 file interface
import h5py

# import Matplotlib graphing package
import matplotlib as mpl
from matplotlib import pyplot as plt #this is the traditional method

mpl.rc('xtick', labelsize = 18)      #use 18 point font for numbering on x axis
mpl.rc('ytick', labelsize = 18)      #use 18 point font for numbering on y axis


# ANALYZE DATA

# import and graph calibration data
CT_cal_data = np.loadtxt('Smith Cernox 1010 X65735LF.txt', skiprows =10)
T_CT_cal_data, R_CT_cal_data = CT_cal_data[:,0], CT_cal_data[:,1]

T_CT_limits = np.array([np.min(T_CT_cal_data), np.max(T_CT_cal_data)])
R_CT_limits = np.array([np.min(R_CT_cal_data), np.max(R_CT_cal_data)])

print('temperature range of calibration data [K]', T_CT_limits)
print('resistance range of calibration data [ohms]', R_CT_limits, '\n')

plt.figure(figsize = (8,8))
plt.plot(T_CT_cal_data, R_CT_cal_data, 'b.', label = 'calibration data')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Temperature [K]',  fontsize = 18)
plt.ylabel('Resistance [$\Omega$]',  fontsize = 18)
# plt.xlim(0.040, 400)
# plt.ylim(10, 1E6)
plt.legend(loc = 'best')
plt.grid(True)

# exectute fit
CT_domain = fn.set_domain(0.050, 335) # defines lower and upper bounds of fit, in Kelvin
CT_fit = fn.fit_data(T_CT_cal_data, R_CT_cal_data,CT_domain)  # try automatic fit
CT_fit = fn.fit_Chebyshev_to_data(T_CT_cal_data, R_CT_cal_data, 8, CT_domain) # compare to manual fit
print("CT_fit.coeff:",CT_fit.coef)
print("min and max temps",np.exp(CT_fit.domain)) # verify min and max temperature values for which fit is defined 
print("full Chebyshev CT_fit:", CT_fit, '\n') # reveal full information stored in Chebyshev class

test_fit = fn.fit_Chebyshev_to_data(T_CT_cal_data, R_CT_cal_data, 9, CT_domain) # what happens if increase order of fit?
print("test_fit.coeff:",test_fit.coef)
print("series_converges =", fn.series_converges_in_value(test_fit.coef))
print("series_alternates_sign =", fn.series_alternates_sign(test_fit.coef), '\n')

# generate temperature and resistance arrays with linearly spaced data points over the full temperature domain
CT_lnT, CT_lnR = CT_fit.linspace(n=100) #nifty function! 
CT_T_fit = np.exp(CT_lnT)
CT_R_fit = np.exp(CT_lnR)

# generate plot
plt.figure(figsize = (8,8))
plt.plot(T_CT_cal_data, R_CT_cal_data, 'b.', label = 'calibration data')
plt.plot(CT_T_fit, CT_R_fit, 'r', label = 'Chebyshev fit')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('Resistance [$\Omega$]', fontsize = 18)
# plt.xlim(0.020, 500)
# plt.ylim(10, 1E8)
plt.legend(loc = 'best', fontsize = 19)
plt.grid(True)


# ASSESS FIT

# determine delta R / R
R_calc = fn.TtoR(T_CT_cal_data, CT_fit)
# print(R_calc)
fractional_error_in_R = (R_calc - R_CT_cal_data)/ R_CT_cal_data

plt.figure(figsize = (8,8))
plt.plot(T_CT_cal_data, fractional_error_in_R, 'b.', label = 'fractional error in fit to data')
plt.xscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('$\Delta R/R$', fontsize = 18)
plt.legend(loc = 'best',  fontsize = 18)
plt.grid(True)

# determine delta T / T
T_calc = fn.RtoT(R_CT_cal_data, CT_fit)
fractional_error_in_T = (T_calc - T_CT_cal_data)/ T_CT_cal_data

plt.figure(figsize = (8,8))
plt.plot(T_CT_cal_data, fractional_error_in_T * 100, 'b.', label = 'percent error in fit to data')
plt.xscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('$\Delta T/T\ [\%]$', fontsize = 18)
plt.legend(loc = 'best',  fontsize = 18)
# plt.xlim(0.020, 500)
plt.ylim(-1,1)
plt.grid(True)


# FIT DIMENSIONLESS SENSITIVITY

derivative_fit = CT_fit.deriv()  # dlnR/dlnT
print("derivative fit:",derivative_fit, '\n')
sensitivity =  derivative_fit(np.log(T_CT_cal_data))

# generate temperature and resistance arrays with linearly spaced data points over the full temperature domain
CT_lnT, eta_fit = derivative_fit.linspace(n=100) #nifty function! 
CT_T_fit = np.exp(CT_lnT)

# generate plot
plt.figure(figsize = (8,8))
plt.plot(T_CT_cal_data, sensitivity, 'b.', label = 'calibration data')
plt.plot(CT_T_fit, eta_fit, 'k', label = 'Chebyshev fit')
plt.xscale('log')
# plt.yscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('dimensionless sensitivity  $dlnR/dlnT$', fontsize = 18)
# plt.xlim(0.020, 500)
plt.ylim(-5, 0)
plt.legend(loc = 'best', fontsize = 19)
plt.grid(True)


# SAVE RESULTS

# CT calibration using LS zero field data for 0.080 to 335 K' 
output_file_name = 'CT_CX1010_SN_X65735LF_50mK_335K'
hdf5_output_file = output_file_name + '.hdf5'

with h5py.File(hdf5_output_file, "w") as f_out:
    grp1 = f_out.create_group("CT_fit")
    dset1a = grp1.create_dataset("CT coefficients", data=CT_fit.coef)
    dset1b = grp1.create_dataset("CT domain", data=CT_fit.domain)
    dset1c = grp1.create_dataset("CT window", data=CT_fit.window)
    
    grp2 = f_out.create_group("derivative_fit")
    dset2a = grp2.create_dataset("sensitivity coefficients",data=derivative_fit.coef)
    dset2b = grp2.create_dataset("sensitivity domain", data=derivative_fit.domain)
    dset2c = grp2.create_dataset("sensitivity window", data=derivative_fit.window)
    

# check that everything loaded properly
with h5py.File(hdf5_output_file, "r") as f_out:
    print(f_out.filename)
    print(f_out.visit(fn.printname), '\n')
    
    test_ds1a = f_out["CT_fit"]["CT coefficients"][:]
    test_ds1b = f_out["CT_fit"]["CT domain"][:]
    test_ds1c = f_out["CT_fit"]["CT window"][:]
    
    test_ds2a = f_out["derivative_fit"]["sensitivity coefficients"][:]
    test_ds2b = f_out["derivative_fit"]["sensitivity domain"][:]
    test_ds2c = f_out["derivative_fit"]["sensitivity window"][:]
    
print("CT coefficients", '\n', test_ds1a, '\n')
print("CT domain", '\n', test_ds1b, '\n')
print("CT window", '\n', test_ds1c, '\n')

print("sensitivity coefficients", '\n', test_ds2a, '\n')
print("sensitivity domain", '\n', test_ds2b, '\n')
print("sensitivity window", '\n', test_ds2c, '\n')
