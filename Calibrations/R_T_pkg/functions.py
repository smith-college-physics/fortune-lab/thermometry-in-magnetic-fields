# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 12:57:06 2020
User-defined functions
@author: jafro
"""

# import numerical Python Spackage
import numpy as np

# import Chebyshev polynomial routines
from numpy.polynomial import Chebyshev
import numpy.polynomial.polynomial as poly

# import nonlinear curve fitting routine from SciPy package
from scipy.optimize import curve_fit


# function to print names of groups, datasets, etc. w/in hdf5 files
def printname(name):
    """
    print input name
    can print internal file hierarchy when used with h5py visit method
    """
    print (name)

def set_domain(T_min, T_max):
    """set range of temperatures for which fit is to be done"""
    domain = np.array([T_min,T_max])
    return domain

def fit_Chebyshev_to_data(T_data, R_data, N, T_domain):
    """find coefficients for an Nth order logR = f(x(logT)) Chebyshev fit """
    fit = Chebyshev.fit(np.log(T_data),np.log(R_data), N, domain = np.log(T_domain))
    return fit   

def PtoT(P_data, Chebyshev_fit):
    """use fit to calculate T from P"""
    lnT = Chebyshev_fit(np.log(P_data))
    T_values = np.exp(lnT)
    return T_values

def TtoR(T_values, Chebyshev_fit):
    """use fit to calculate R from T"""
    lnR = Chebyshev_fit(np.log(T_values))
    R_values = np.exp(lnR)
    return R_values

def RtoT(R_values, Chebyshev_fit):
    """use fit to calculate T from R for set of R_values
    by finding roots of Chebyshev series for modified coefficient
    root_fit.coef[0]= Chebyshev_fit.coef[0] - np.log(R_value) 
    then return as a set of T_values"""
    
    # fix if user inputs a scalar value instead of an array for R_values
    if np.isscalar(R_values): 
        values = np.array([R_values])
    else:
        values = R_values
    
    # prepare to find roots
    root_fit = Chebyshev_fit.copy()  #make a copy so as not to modify the original
    T_values = np.empty(values.size) #create an empty array to fill with T_values

    for index, value in enumerate(values): 
        root_fit.coef[0] = Chebyshev_fit.coef[0] - np.log(value) 
        roots = root_fit.roots()  # compute roots for R value
    
        #sort roots, find root that is real and within T domain
        T_solve = [np.real(np.exp(root)) for root in roots if ((root < Chebyshev_fit.domain.max() and root > Chebyshev_fit.domain.min()) and np.isreal(root))]

        #check that there is one and only one real root
        if len(T_solve) == 1: 
            T_value = T_solve[0] 
        elif len(T_solve) == 0:
            print('error! no real roots for R = ', value)
            T_value = np.nan
        else:
            print('warning! multiple real roots in domain for R =', value)
            print(T_solve)
            T_value = np.nan 
        # assign T_value to array
        T_values[index]   = T_value   # assign T value 
    return T_values


# define functions for convenience
def series_alternates_sign(coefficient_array):
    """determine whether the elements of the input series alternate sign"""
    test = np.all(np.sign(coefficient_array[1:]) + np.sign(coefficient_array[:-1]) ==0 )
    return test

def series_converges_in_value(coefficient_array):
    """determine whether the input series converge in value"""
    if np.all(np.abs(coefficient_array[1:]) < np.abs(coefficient_array[:-1])):
        test = True
    else: test = np.all(np.abs(coefficient_array[2:]) < np.abs(coefficient_array[1:-1]))
    return test

def fit_data(T_data, R_data, T_domain, N_max = 20, N_initial = 2):
    """choose largest N for which series converges in value"""
    N = N_initial
    test_fit = fit_Chebyshev_to_data(T_data, R_data, N, T_domain)
    if series_converges_in_value(test_fit.coef) == False: 
        print("no convergence")
        fit = 0
    else:
        while (series_converges_in_value(test_fit.coef)):
            fit = test_fit
            N = N + 1
            test_fit = fit_Chebyshev_to_data(T_data, R_data, N, T_domain)
       
        print('fit order = ', N -1)
        print('series coefficients converge?', series_converges_in_value(fit.coef)) 
        print('coefficients alternate sign? ', series_alternates_sign(fit.coef))
        print('coefficients: ')
        print(fit.coef, '\n')
    return fit
   

def process_power_ramp_data(fields, P_to_T, temperature_range, sensor = 'CT', order = 5):
    """ convert P_PH values to T_sensor values, then do Chebyshev fit of R_sensor to T_sensor
    
    Parameters
    ----------
    fields : array
        specifies magnetic field values at which power ramp data was taken
    P_to_T : Chebyshev object ( coefficients, domain, window)
        specifies fit used to convert heater power to T
    temperature_range : array
        domain for lnR(lnT) fit
    order : int
        maximum order for fit. default is 5
    sensor : str
        specifies name of sensor (CT, PT, ST). default is 'CT'
        
    Returns
    -------
    fit_array : array
        array containing magnetic field values and Chebyshev fits
    
    Note
    ----
    the array using a custom dtype could be rewritten using a dictionary
    
    """
    
    power_ramp_fields = np.asarray(fields)
    
    #create structured_array for output
    data_structure = np.dtype([('B_value', np.float64), ('B_fit', np.object),])
    
    # fill with results from zero field data
    input_file = str(sensor + '_') + '0' + '_kG.txt'
    print(input_file, '\n')
    power, resistance = np.loadtxt(input_file, skiprows =1, delimiter=',', unpack = True)  # load data
    temperature = np.exp(P_to_T(np.log(power)))
    fit = fit_Chebyshev_to_data(temperature, resistance, order, temperature_range)
    fit_array = np.array([(0, fit)], dtype = data_structure)
    
    # cycle through non-zero field data 
    # generate file name (might be better to load fields and file names from a spreadsheet file)
    # calculate temperatures and generate Chebyshev coefficients for lnR(lnT) 
    for field in power_ramp_fields[1:]:
        field_index = field
        field_string_in_kG = "%d" %field_index
        input_file = str(sensor + '_') + field_string_in_kG + '_kG.txt'
        print(input_file, '\n')
        
        power, resistance = np.loadtxt(input_file, skiprows =1, delimiter=',', unpack = True)  # load data
        temperature = np.exp(P_to_T(np.log(power)))
        fit = fit_Chebyshev_to_data(temperature, resistance, order, temperature_range)
        latest_fit_array = np.array([(field, fit)], dtype = data_structure)
        fit_array = np.append(fit_array, latest_fit_array)
        
        lnT, lnR = fit.linspace(n=100)
        T_fit = np.exp(lnT)
        R_fit = np.exp(lnR)
        title = 'CT'+ field_string_in_kG + 'kG'
    
    return fit_array  


# define function for generation of field array
def field_array(field_value, matrix):
    """
    create an array ([B^0, B^1, B^2,...,B^N]) from field value B
    where N is the number of columns in the provided matrix
    """
    field_power = np.arange(0, np.shape(matrix)[1], 1)
    return field_value ** field_power

# define function for matrix calculation of c_n(B)
def field_coefficients(field_value, c_0_coefficients, kappa, gamma):
    """generate array of $c_n(B)$ coefficients for R(T) and T(R)"""
 
    # make a copy of the coefficients so as not to change the original
    c_array = c_0_coefficients.copy()
    
    # create the field arrays (1, B, B**2, ....)
    B_kappa = field_array(field_value, kappa)
    B_gamma = field_array(field_value, gamma)
    
    numerator_array = np.matmul(kappa, B_kappa)
    # matrix multiplication of P X 1 matrix (1, B, ...,B**P) 
    # by N x P matrix (kappa) 
    # yields a N x 1 matrix (1D array)
    
    denominator_array = np.matmul(gamma, B_gamma)
    # matrix multiplication of Q X 1 matrix (1, B, ...,B**Q) 
    # by N x Q matrix (gamma) 
    # yields a N x 1 matrix (1D array)
    
    y_array = numerator_array / denominator_array 
    # reminder: array division is element by element
    
    z_array = 1 + y_array
    # reminder: here 1 is treated as an array of ones of length y_array 
    
    c_array = c_0_coefficients * z_array
    # reminder: 1D array multiplication is element by element. 
    # This is not the dot product np.dot
        
    return c_array


# define final result functions

# T --> R for constant value of B
def TandBtoR(T_values,  CT_zero_field_fit, kappa, gamma, B_value = 0):
    """calculate R from T and B using Chebyshev_fit"""
  
    c_0_array = CT_zero_field_fit.coef
    CT_field_fit = CT_zero_field_fit.copy()
    CT_field_fit.coef = field_coefficients(B_value, c_0_array, kappa, gamma)
    
    return TtoR(T_values, CT_field_fit)

# R --> T for constant value of B
def RandBtoT(R_values, CT_zero_field_fit, kappa, gamma, B_value = 0):
    """calculate T from R and B using Chebyshev_fit"""
  
    c_0_array = CT_zero_field_fit.coef
    CT_field_fit = CT_zero_field_fit.copy()
    CT_field_fit.coef = field_coefficients(B_value, c_0_array, kappa, gamma)
    
    return RtoT(R_values, CT_field_fit)

# R --> T for variable B 
def calculate_T_from_data(R_values, B_values, zero_field_fit, kappa, gamma):
    """calculate T from (R,B) data arrays using Chebyshev fit to resistive thermometer"""
    
    # initialize T array
    T_values = np.zeros(R_values.size)
    
    # calculate T from R, B
    for index, temperature in enumerate(T_values): 
        temperature = RandBtoT(R_values[index], zero_field_fit,kappa, gamma, B_values[index])
        T_values[index] = temperature

    return T_values
