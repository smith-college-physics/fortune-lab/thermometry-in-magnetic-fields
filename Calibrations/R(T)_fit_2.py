# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 08:41:00 2020
From Professor Fortune's file R(T)_fit_part_2
@author: jafro
"""

# IMPORT PYTHON PACKAGES

# import custom functions
import R_T_pkg as rtpckg
from R_T_pkg import functions as fn

# import numerical Python Spackage
import numpy as np

# import Chebyshev polynomial routines
from numpy.polynomial import Chebyshev
# import numpy.polynomial.polynomial as poly

# import nonlinear curve fitting routine from SciPy package
from scipy.optimize import curve_fit

# import data packing routine for object arrays
#import pickle

# import HDF5 file interface
import h5py

# import Matplotlib graphing package
import matplotlib as mpl
from matplotlib import pyplot as plt #this is the traditional method

mpl.rc('xtick', labelsize = 18)      #use 18 point font for numbering on x axis
mpl.rc('ytick', labelsize = 18)      #use 18 point font for numbering on y axis


# ANALYZE DATA
    
# import and graph calibration data
P_PH, R_CT = np.loadtxt('CT_0_kG.txt', skiprows =1, delimiter=',', unpack = True)

P_CT_limits = np.array([np.min(P_PH), np.max(P_PH)])
R_CT_limits = np.array([np.min(R_CT), np.max(R_CT)])

print(' range of platform heater powers: [' , P_CT_limits[0],',', P_CT_limits[1], '] Watts')
print('resistance range of calibration data: [', R_CT_limits[0],',', R_CT_limits[1], '] Ohms', '\n')

plt.figure(figsize = (6,6))
plt.plot(P_PH, R_CT, 'b', label = 'calibration data')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Power [W]',  fontsize = 18)
plt.ylabel('Resistance [$\Omega$]',  fontsize = 18)
# plt.xlim(0.040, 400)
# plt.ylim(10, 1E6)
plt.legend(loc = 'best')
plt.grid(True)

# convert to temperature
# CT calibration using zero field data

calibration_file_folder = ''  # if file in same folder as program, else
# calibration_file_folder = 'calibrations/'

calibration_file_name = 'CT_CX1010_SN_X65735LF_50mK_335K'
hdf5_file = calibration_file_name + '.hdf5' 

with h5py.File(hdf5_file, "r") as f_in:
    print(f_in.filename)
    print(f_in.visit(fn.printname), '\n')
    
    ct_coeff = f_in["CT_fit"]["CT coefficients"][:]
    ct_dom = f_in["CT_fit"]["CT domain"][:]
    ct_win = f_in["CT_fit"]["CT window"][:]
    
    CT_fit = Chebyshev(ct_coeff, ct_dom, ct_win)


print("CT_fit.coeff:", '\n', CT_fit.coef)
print("fit domain:",np.exp(CT_fit.domain), 'Kelvin', '\n')

# use  fit to find temperature 
T_CT = fn.RtoT(R_CT, CT_fit)
T_CT_limits = np.array([np.min(T_CT), np.max(T_CT)])
print("T_CT_limits:",T_CT_limits, 'Kelvin', '\n')

plt.figure(figsize = (6,6))
plt.plot(P_PH, T_CT, 'r', label = 'calibration data')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Power [W]',  fontsize = 18)
plt.ylabel('Temperature [K]',  fontsize = 18)
plt.xlim(1E-9, 10E-3)
plt.ylim(0.020, 20)
plt.legend(loc = 'best')
plt.grid(True, which = 'both')


# EXECUTE FIT
# use largest order for which values converge (except for first point)
PH_domain = np.array([5E-9, 1E-2])
PH_fit = Chebyshev.fit(np.log(P_PH),np.log(T_CT), 7, np.log(PH_domain))
PH_lnP, PH_lnT = PH_fit.linspace(n=100)
P_PH_fit = np.exp(PH_lnP)
T_PH_fit = np.exp(PH_lnT)

print('series converges?', fn.series_converges_in_value(PH_fit.coef[1:]))
print("PH_fit.coeff:",PH_fit.coef, '\n')

# display results
plt.figure(figsize = (6,6))
plt.plot(P_PH, T_CT, 'b.', markevery = 250, label = 'data')
plt.plot(P_PH_fit, T_PH_fit, 'r', label = 'fit')
plt.xscale('log')
plt.yscale('log')
plt.xlabel('Power [W]', fontsize = 18)
plt.ylabel('Temperature [K]', fontsize = 18)

plt.xlim(2E-9, 2E-2)
plt.ylim(0.05, 20)
plt.legend(loc = 'best',  fontsize = 18)
plt.grid(True)


# ASSESS FIT

T_PH = np.exp(PH_fit(np.log(P_PH)))
T_PH_fractional_error = (T_PH - T_CT)/T_CT

plt.figure(figsize = (6,6))

plt.plot(T_PH, T_PH_fractional_error, 'r.', label = 'fractional error in fit to data',  markevery = 100)
plt.xscale('log')
plt.xlabel('Temperature [K]', fontsize = 18)
plt.ylabel('$\Delta T/T$' , fontsize = 18)
plt.legend(loc = 'best' , fontsize = 18)
plt.xlim(0.02, 20)
plt.ylim(-0.01, 0.01)
plt.grid(True)


# SAVE T(P_ph) FIT

# output_file_folder = 'calibrations/'
output_file_folder = ''
output_file_name = 'PH_fit_PDFv2_SCM1_2017' 
hdf5_output_file = output_file_name + '.hdf5'

with h5py.File(hdf5_output_file, "w") as f_out:
    grp1 = f_out.create_group("PH_fit")
    dset1a = grp1.create_dataset("PH coefficients", data=PH_fit.coef)
    dset1b = grp1.create_dataset("PH domain", data=PH_fit.domain)
    dset1c = grp1.create_dataset("PH window", data=PH_fit.window)


# check that everything loaded properly
with h5py.File(hdf5_output_file, "r") as f_out:
    print(f_out.filename)
    print(f_out.visit(fn.printname), '\n')
    
    test_ds1a = f_out["PH_fit"]["PH coefficients"][:]
    test_ds1b = f_out["PH_fit"]["PH domain"][:]
    test_ds1c = f_out["PH_fit"]["PH window"][:]

print("PH_fit",'\n',PH_fit, '\n')

print("PH coefficients", '\n', test_ds1a, '\n')
print("PH domain", '\n', test_ds1b, '\n')
print("PH window", '\n', test_ds1c, '\n')
