This folder should contain examples of the resistive thermometer characterization code being used in different situations. Ideally, these should be Jupyter notebooks. Each notebook should contain step-by-step code characterizing a different type of resistive thermometer or applying characterizations to different datasets. The Jupyter notebook format allows for detailed explanations of the different steps and use cases. 

Further work is needed to create these example notebooks.

This folder currently contains a partial code example, RT_1_2_Ba3CoSb2O9.py