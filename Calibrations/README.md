This folder contains code and data files to characterize a resistive thermometer in non-zero magnetic fields. For more information and explanation of how to use the code, please see the project wiki.

Folder contents:
- Examples folder - contains example programs applying code to different datasets and thermometers. Further work needed.
- R_T_pkg folder - package with custom functions and Datafit class to improve code efficiency
- CT_B_kG.txt data files - resistance and power data for different magnetic field strengths
- hdf5 files - contain calibration and Chebyshev fit information for the resistive thermometer
- R(T)_fit_1_and_2.py - code to generate and store Chebyshev fits in zero field between temperature and resistance and between power and resistance
- R(T)_fit_3.py - code that uses the fits from R(T)_fit_1_and_2.py to characterize how magnetic field strength impacts the thermometer
- R(T)_fit_1.py - out of date (replaced by R(T)_fit_1_and_2.py)
- R(T)_fit_2.py - out of date (replaced by R(T)_fit_1_and_2.py)
- Smith Cernox 1010 X65735LF.txt - contains zero-field calibration curve for a specific resistive thermometer
- fits.hdf5 - contains Chebyshev fits generated in R(T)_fit_1_and_2.py and used in R(T)_fit_3.py